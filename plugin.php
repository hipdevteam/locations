<?php
/*
Plugin Name: Hip Locations CPT
Description: Simple, flexible, location post type for Hip Marketing Sites, including nice little map module.
Author: Hip Creative
Version: 1.2.4
*/

if (file_exists(__DIR__ . '/vendor/autoload.php'))
    require __DIR__ . '/vendor/autoload.php';

function hip_location_cpt_init()
{
    $app = new Hip\Location\Plugin();
    $app->run();
    define('HIP_LOCATION_PLUGIN_URL', $app['plugin_url']);
}
add_action('plugins_loaded', 'hip_location_cpt_init');
