# Hip Locations Plugin

This plugin adds a `location` post type to WordPress, along with templates for
listing all locations and displaying individual locations.

## Setup

Upon activation, a Google Map API key will need to be added before any maps will work.
The key can be added in the WP Admin -> Locations -> Settings page under the General tab.

## Shortcodes

`[ location_module ]` - Renders a page section listing all locations, including
a map.

`[ location_module_without_map ]` - Renders a page section listing all
locations; no map.

`[ hip_location_map ]` - Renders a map with pins for all locations.

`[ hip_single_location_map ]` - Renders a map with a pin for a single location with a placecard.
This shortcode takes a required parameter `slug` that takes the the slug of the desired location.
Optional parameters of `zoom` (default is 15) and `height` (default is 360px) are accepted.
Ex: `[hip_single_location_map slug="hip" zoom="12" height="500px"]`

## Overriding Single Location Template

A default template for displaying a single location is included. This template
displays a Google Map of the location with placecard, the address of the
location, a link to review the location on Google, hours of operation, and
a place for free-form additional information. It also takes advantage of
several settings that can be controlled from the WP Admin.

If this template does not meet your needs, you can create your own template
in your theme or a seperate plugin. The following code can be placed in
your functions.php file to override the template file used:

```
add_filter( 'single_template', function($template) {
    global $post;
    if ( $post->post_type == 'location' ) {
        // Replace with the path to your custom template file
        $template = get_stylesheet_directory() . '/inc/location.php'
    }

    return $template;
}, 11 );
```