<?php

namespace Hip\Location;

class Admin
{

	public function savePostMeta($post_id)
	{
		if (!isset($_POST['meta_box_nonce_key']) || !wp_verify_nonce($_POST['meta_box_nonce_key'], 'hip_location_cpt_meta_box_nonce')) {
			return true;
		}

		if (!current_user_can("edit_post", $post_id)) {
			return $post_id;
		}
		if (defined("DOING_AUTOSAVE") && DOING_AUTOSAVE) {
			return $post_id;
		}

		// Only fields that can safely be passed into santize_text_field filter
		$array = array(
			'location_show_in_footer',
			'location_office_info',
			'hip_location_address',
			'location_lng',
			'location_lat',
			'location_street_address',
			'location_city',
			'location_state',
			'state_option',
			'location_zipcode',
			'location_phone_number',
			'location_phone_new_patient',
			'location_texting',
			'gmb_url',
			'gmb_url_mobile',
			'location_place_id',
			'location_place_url',
			'location_facebook_url',
			'demandforce_id',
			'demandforce_slug',
		);

		foreach ($array as $key) {
			if (isset($_POST[$key])) {
				update_post_meta($post_id, $key, sanitize_text_field($_POST[$key]));
			}
		}

		if (isset($_POST['after_location_content'])) {
			$this->location_hours = stripslashes($_POST['after_location_content']);
		}
		update_post_meta($post_id, 'after_location_content', $this->location_hours);
		
		if (isset($_POST['location_hours'])) {
			$this->location_hours = trim($_POST['location_hours']);
		}
		update_post_meta($post_id, 'location_hours', $this->location_hours);

		if (isset($_POST['location_infowindow'])) {
			$this->location_infowindow = trim($_POST['location_infowindow']);
		}
		update_post_meta($post_id, 'location_infowindow', $this->location_infowindow);
	}

	public function addMetaBox()
	{
		add_meta_box(
			'hip-location-metabox',
			'Location Options',
			[$this, 'renderMetaBox'],
			'location',
			'normal',
			'high'
		);

    add_meta_box('hip-location-after-content-metabox',
      'After Location Info',
      [$this, 'renderAfterContentMetabox'],
      'location',
      'normal',
      'high'
    );
	}

  public function renderAfterContentMetabox($post) {
      wp_editor(get_post_meta($post->ID, 'after_location_content', true), 'after_location_content', array(
			'wpautop' => false,
	));
  }

	public function renderMetaBox($post)
	{
		wp_nonce_field('hip_location_cpt_meta_box_nonce', 'meta_box_nonce_key');
?>

		<div class="hip-street-address">
			<p><label for="location_show_in_footer"><b>I want to show this location in footer</b>&nbsp;&nbsp;
					<p><label><input name="location_show_in_footer" id="location_show_in_footer" type="radio" value="show_in_footer" <?php if (get_post_meta($post->ID, 'location_show_in_footer', true) == 'show_in_footer' || ((get_post_meta($post->ID, 'location_show_in_footer', true) == ''))) {
																																												echo 'checked';
																																											}; ?> />Yes</label></p>
					<p><label><input name="location_show_in_footer" id="location_show_in_footer" type="radio" value="do_not_show_in_footer" <?php if ((get_post_meta($post->ID, 'location_show_in_footer', true) === 'do_not_show_in_footer')) {
																																														echo 'checked';
																																													}; ?> />No</label></p>
		</div>

		<div class="hip-address">
			<p><label for="hip_location_address"><b>Search Google for Location</b></label></p>
			<p><input name="hip_location_address" id="hip_location_address" type="text" class="large-text" value="<?php echo get_post_meta($post->ID, 'hip_location_address', true); ?>" /></p>
		</div>

		<!-- location maps autocomplete hidden fields -->
		<input type="hidden" class="field" id="street_number">
		<input type="hidden" class="field" id="route">
		<input type="hidden" class="field" id="administrative_area_level_1">
		<input type="hidden" class="field" id="locality">
		<input type="hidden" class="field" id="country">

		<div class="hip-archive-office-info">
			<p><label for="location_office_info"><b>Archive Office Info (Appears in Title after city)</b></label></p>
			<p><input name="location_office_info" id="location_office_info" type="text" class="large-text" value="<?php echo get_post_meta($post->ID, 'location_office_info', true); ?>" /></p>
		</div>

		<div class="hip-lat">
			<p><label for="hip_lng"><b>Latitude</b></label></p>
			<p><input name="location_lat" id="hip_lat" type="text" class="large-text" value="<?php echo get_post_meta($post->ID, 'location_lat', true); ?>" /></p>
		</div>
		<div class="hip-lng">
			<p><label for="hip_lng"><b>Longitude</b></label></p>
			<p><input name="location_lng" id="hip_lng" type="text" class="large-text" value="<?php echo get_post_meta($post->ID, 'location_lng', true); ?>" /></p>
		</div>


		<div class="hip-street-address">
			<p><label for="hip-street-address"><b>Street Address</b></label></p>
			<p><input name="location_street_address" id="hip_street_address" type="text" class="large-text" value="<?php echo get_post_meta($post->ID, 'location_street_address', true); ?>" /></p>
		</div>



		<div class="hip-city">
			<p><label for="hip-city"><b>City</b></label></p>
			<p><input name="location_city" id="hip_city" type="text" class="large-text" value="<?php echo get_post_meta($post->ID, 'location_city', true); ?>" /></p>
		</div>

		<div class="hip-state">
			<p><label for="hip_state"><b>State</b></label></p>
			<p><input name="location_state" id="hip_state" type="text" class="large-text" value="<?php echo get_post_meta($post->ID, 'location_state', true); ?>" /></p>
		</div>

		<div class="hip-state-option">
			<p><label for="hip_state_option"><b>Show state in footer</b></label></p>
			<p><label><input name="state_option" id="hip_state_option" type="radio" value="show_state" <?php if (get_post_meta($post->ID, 'state_option', true) == 'show_state') {
																																			echo 'checked';
																																		}; ?> />Show state in footer</label></p>
			<p><label><input name="state_option" id="hip_state_option" type="radio" value="do_not_show_state" <?php if ((get_post_meta($post->ID, 'state_option', true) == 'do_not_show_state') || ((get_post_meta($post->ID, 'state_option', true) == ''))) {
																																					echo 'checked';
																																				}; ?> />Do not show state in footer</label></p>
		</div>

		<div class="hip-zipcode">
			<p><label for="hip_zipcode"><b>Zip Code</b></label></p>
			<p><input name="location_zipcode" id="postal_code" type="text" class="large-text" value="<?php echo get_post_meta($post->ID, 'location_zipcode', true); ?>" /></p>
		</div>

		<div class="hip-phone-number">
			<p><label for="hip_phone_number"><b>Phone Number</b></label></p>
			<p><input name="location_phone_number" id="hip_phone_number" type="text" class="large-text" value="<?php echo get_post_meta($post->ID, 'location_phone_number', true); ?>" /></p>
		</div>

		<div class="hip-phone-new-patient">
			<p><label for="location_phone_new_patient"><b>New patient line:</b></label></p>
			<p><input name="location_phone_new_patient" id="location_phone_new_patient" type="text" class="large-text" value="<?php echo get_post_meta($post->ID, 'location_phone_new_patient', true); ?>" /></p>
		</div>

		<div class="hip-texting">
			<p><label for="location_texting"><b>Does this branch allow texting?</b></label></p>
			<p><label><input name="location_texting" id="location_texting" type="radio" value="yes" <?php if (get_post_meta($post->ID, 'location_texting', true) == 'yes') {
																																		echo 'checked';
																																	}; ?> />Yes</label></p>
			<p><label><input name="location_texting" id="location_texting" type="radio" value="no" <?php if ((get_post_meta($post->ID, 'location_texting', true) == 'no') || ((get_post_meta($post->ID, 'location_texting', true) == ''))) {
																																	echo 'checked';
																																}; ?> />No</label></p>
		</div>

		<div class="hip-place-id">
			<p><label for="hip-place-id"><b>Google Place ID</b></label></p>
			<p><input name="location_place_id" id="hip_place_id" type="text" class="large-text" value="<?php echo get_post_meta($post->ID, 'location_place_id', true); ?>" /></p>
		</div>

		<div class="hip-place-url">
			<p><label for="hip-place-id"><b>Google Place URL</b></label></p>
			<p><input name="location_place_url" id="hip_place_url" type="text" class="large-text" value="<?php echo get_post_meta($post->ID, 'location_place_url', true); ?>" /></p>
		</div>

		<div class="hip-gmb-url">
			<p><label for="hip_gmb_url"><b>Google My Business Review URL</b></label></p>
			<p><input name="gmb_url" id="hip_gmb_url" type="text" class="large-text" value="<?php echo get_post_meta($post->ID, 'gmb_url', true); ?>" /></p>
		</div>

		<div class="hip-gmb-url-mobile">
			<p><label for="hip_gmb_url_mobile"><b>Google My Business Review URL for Mobile</b></label></p>
			<p><input name="gmb_url_mobile" id="hip_gmb_url_mobile" type="text" class="large-text" value="<?php echo get_post_meta($post->ID, 'gmb_url_mobile', true); ?>" /></p>
		</div>

		<div>
			<p><label for="location_facebook_url"><b>Facebook Reviews URL</b></label></p>
			<p><input name="location_facebook_url" id="location_facebook_url" type="text" class="large-text" value="<?php echo get_post_meta($post->ID, 'location_facebook_url', true); ?>" /></p>
		</div>

		<div class="hip-houses-of-operation">
			<p><label for="hip-houses-of-operation"><b>Hours of Operation</b></label></p>
			<p><textarea class="large-text" id="hip-houses-of-operation" rows="4" cols="53" name="location_hours"><?php echo get_post_meta($post->ID, 'location_hours', true); ?></textarea></p>
		</div>

		<div class="hip-infowindow">
			<p><label for="hip-infowindow"><b>Map Info Window</b></label></p>
			<p><textarea class="large-text" id="hip-infowindow" rows="4" cols="53" name="location_infowindow"><?php echo get_post_meta($post->ID, 'location_infowindow', true); ?></textarea></p>
		</div>

		<div>
			<p><label for="demandforce_id"><b>DemandForce ID</b></label></p>
			<p><input name="demandforce_id" id="demandforce_id" type="text" class="regular-text" value="<?php echo get_post_meta($post->ID, 'demandforce_id', true); ?>" /></p>
		</div>
		<div>
			<p><label for="demandforce_slug"><b>DemandForce Slug</b></label></p>
			<p><input name="demandforce_slug" id="demandforce_slug" type="text" class="large-text" value="<?php echo get_post_meta($post->ID, 'demandforce_slug', true); ?>" /></p>
		</div>

		<script>
			var autocomplete, componentForm = {
				street_number: 'short_name',
				route: 'long_name',
				locality: 'long_name',
				administrative_area_level_1: 'short_name',
				country: 'long_name',
				postal_code: 'short_name'
			};

			function initAutocomplete() {
				var input = document.getElementById('hip_location_address');

				autocomplete = new google.maps.places.Autocomplete(input);

				autocomplete.addListener('place_changed', fillMetaboxsfields);
			}

			function fillMetaboxsfields() {

				var place = autocomplete.getPlace();

				for (var component in componentForm) {
					document.getElementById(component).value = '';
				}

				for (var i = 0; i < place.address_components.length; i++) {
					var addressType = place.address_components[i].types[0];
					if (componentForm[addressType]) {
						var val = place.address_components[i][componentForm[addressType]];
						document.getElementById(addressType).value = val;
					}
				}

				document.getElementById('hip_lng').value = place.geometry.location.lng();
				document.getElementById('hip_lat').value = place.geometry.location.lat();

				document.getElementById('hip_street_address').value =
					place.address_components[0]['long_name'] + ' ' +
					place.address_components[1]['short_name'] + ' ' +
					place.address_components[2]['short_name'];

				document.getElementById('hip_city').value =
					place.address_components[3]['long_name'];

				document.getElementById('hip_state').value =
					place.address_components[5]['short_name'];

				document.getElementById('hip_place_url').value =
					place.url;

				document.getElementById('hip_place_id').value = place.place_id;

				document.getElementById('hip_gmb_url').value =
					'https://search.google.com/local/writereview?placeid=' +
					place.place_id;

				document.getElementById('hip_gmb_url_mobile').value =
					'https://search.google.com/local/writereview?placeid=' +
					place.place_id;
			}
		</script>

<?php
		$location_settings = '';

		if (class_exists('Hip\Location\Settings')) {
			$location_settings = \Hip\Location\Settings::getSettings();
		}

		if (!empty($location_settings['google_api_key'])) {
			$api = sprintf('https://maps.googleapis.com/maps/api/js?key=%1$s&callback=initAutocomplete&libraries=places', $location_settings['google_api_key']);
			wp_enqueue_script(
				'google-maps-api',
				$api,
				[],
				$version
			);
		}
	}
}
