<?php

namespace Hip\Location;

use Pimple\Container;

class Plugin extends Container
{
	/* Post Type */
	public $post_type = 'location';

	public function __construct()
	{
		$this['slug'] = 'location';
		$this['version'] = '1.2.4';
		$this['plugin_dir'] = dirname(__DIR__);
		$this['plugin_url'] = plugins_url('', dirname(__FILE__));

		$this['frontend'] = function ($container) {
			return new Frontend();
		};

		$this['admin'] = function ($container) {
			return new Admin();
		};

		$this['settings'] = function ($container) {
			return new Settings();
		};

		$this['settings_page'] = function ($container) {
			return new SettingsPage(['post_type' => $this->post_type, 'plugin_url' => $this['plugin_url']]);
		};

		$this['api'] = function ($container) {
			return new API();
		};

		new GutenbergBlock(['post_type' => $this->post_type, 'plugin_dir' => $this['plugin_dir']]);
	}

	public function run()
	{
		add_action('init', [$this, 'register_post_type']);

		add_action('wp_enqueue_scripts', function () {
			$this['frontend']->enqueueScriptsAndStyles(
				$this['plugin_url'],
				$this['version']
			);
		});

		add_action('add_meta_boxes', function () {
			$this['admin']->addMetaBox();
		});

		add_action('save_post', function ($post_id) {
			$this['admin']->savePostMeta($post_id);
		});

		add_filter('single_template', function ($template) {
			return $this['frontend']->singleTemplate($template, $this->post_type, $this['plugin_dir']);
		});

		add_shortcode('location_module', function () {
			return $this['frontend']->renderMultiLocationMap($this->post_type, $this['plugin_dir']);
		});

		add_shortcode('location_module_without_map', function () {
			return $this['frontend']->renderMultiLocationWithoutMap($this->post_type, $this['plugin_dir']);
		});

		add_shortcode('hip_location_map', function () {
			return $this['frontend']->renderMapOnly(
				$this->post_type,
				$this['plugin_dir']
			);
		});

		add_shortcode('hip_location_maponly', function () {
			return $this['frontend']->renderSingleLocationMapOnly(
					$this['plugin_dir']
			);
		});

		add_shortcode('hip_single_location_map', function($atts) {
			return $this['frontend']->renderSingleMapOnly($this->post_type, $this['plugin_dir'], $atts['slug'], $atts['zoom'], $atts['height'], $atts['width']);
		});

		add_action('admin_menu', function () {
			$this['settings_page']->addOptionsPage();
		});

		add_action('rest_api_init', function () {
			$this['api']->addRoutes();
		});

		add_action('admin_enqueue_scripts', function () {
			$this['settings_page']->enqueueAssets();
		});

		add_action('wp_head', [$this, 'frontend_style_from_location_settings']);

		add_action('wp_footer', function () {
			return $this['frontend']->googleMapsJS();
		});
	}

	public function register_post_type()
	{
		if (class_exists('Hip\Location\Settings')) {
			$location_settings = \Hip\Location\Settings::getSettings();
		}

		$has_archive = (!empty($location_settings['location_has_archive']) ? ($location_settings['location_has_archive'] !== "false") : false);

		$labels = [
			'name'			=> __('Locations', $this['slug']),
			'singular_name' => __('Location', $this['slug']),
			'all_items'		=> __('All Locations', $this['slug']),
			'add_new_item'	=> __('Add New Location', $this['slug']),
			'new_item'		=> __('New Location', $this['slug']),
			'edit_item'		=> __('Edit Location', $this['slug'])
		];

		$args = [
			'description'			=> __('Simple, flexible, add location, maps', $this['slug']),
			'labels'				=> $labels,
			'supports'				=> ['title', 'revisions', 'editor', 'page-attributes'],
			'public'				=> true,
			'show_ui'				=> true,
			'show_in_menu'			=> true,
			'menu_position'			=> 30,
			'show_in_admin_bar'		=> true,
			'show_in_nav_menus'		=> true,
			'can_export'			=> true,
			'publicly_queryable'	=> true,
			'menu_icon'				=> 'dashicons-location',
			'has_archive'			=> $has_archive,
			'rewrite'				=> ['slug' => 'location']
		];

		register_post_type($this->post_type, $args);
		flush_rewrite_rules();
	}

	public function frontend_style_from_location_settings()
	{
		if (class_exists('Hip\Theme\Settings\General')) {
			$general_settings = \Hip\Theme\Settings\General\Settings::getSettings();
		}
		if (class_exists('Hip\Location\Settings')) {
			$location_settings = \Hip\Location\Settings::getSettings();
		}
?>
		<style>
			/*
			 * Location Settings Footer Style
			 */
			<?php if (!empty($location_settings['location_footer_primary_txt'])) : ?>.locations .location-without-maps .footer-primary-color {
				color: <?php echo $location_settings['location_footer_primary_txt']; ?>;
				transition: .3s;
			}

			<?php endif; ?>

			/* Footer CTA Button */
			<?php
			$primaryColor = ((!empty($general_settings['primary_color'])) ? $general_settings['primary_color'] : "inherit");
			$secondaryColor = ((!empty($general_settings['secondary_color'])) ? $general_settings['secondary_color'] : "inherit");
			$buttonBgColor = ((!empty($location_settings['location_footer_button_bg_color'])) ? $location_settings['location_footer_button_bg_color'] : "inherit");
			$buttonPadding = ((!empty($location_settings['location_footer_button_padding'])) ? $location_settings['location_footer_button_padding'] : "15px");
			$buttonMargin = ((!empty($location_settings['location_footer_button_margin'])) ? $location_settings['location_footer_button_margin'] : "15px 0 25px");
			$buttonWidth = ((!empty($location_settings['location_footer_button_width'])) ? $location_settings['location_footer_button_width'] : "200px");
			$buttonBorderRadius = ((!empty($location_settings['location_footer_button_border_radius'])) ? $location_settings['location_footer_button_border_radius'] : "50px");
			$buttonTxtColor = ((!empty($location_settings['location_footer_button_txt_color'])) ? $location_settings['location_footer_button_txt_color'] : "inherit");
			$buttonBorder = ((!empty($location_settings['location_footer_button_border'])) ? $location_settings['location_footer_button_border'] : "0");
			$buttonBottomBorder = ((!empty($location_settings['location_footer_button_bottom_border'])) ? $location_settings['location_footer_button_bottom_border'] : "0");
			$buttonBgColorHover = ((!empty($location_settings['location_footer_button_bg_color_hover'])) ? $location_settings['location_footer_button_bg_color_hover'] : $primaryColor);
			$buttonTxtColorHover = ((!empty($location_settings['location_footer_button_txt_color_hover'])) ? $location_settings['location_footer_button_txt_color_hover'] : $primaryColor);
			?>.footer-primary-call-button {
				background-color: <?php echo $buttonBgColor; ?>;
				padding: <?php echo $buttonPadding; ?>;
				margin: <?php echo $buttonMargin; ?>;
				width: <?php echo $buttonWidth; ?>;
				border-radius: <?php echo $buttonBorderRadius; ?>;
				color: <?php echo $buttonTxtColor; ?>;
				border: <?php echo $buttonBorder; ?>;
				border-bottom: <?php echo $buttonBottomBorder; ?>;
				transition: .3s;
			}

			.footer-primary-call-button:hover {
				background-color: <?php echo $buttonBgColorHover; ?>;
				color: <?php echo $buttonTxtColorHover; ?>;
				transition: .3s;
			}

			/* Footer View More Menu */
			<?php
			$menuBgColor = ((!empty($location_settings['location_footer_menu_bg_color'])) ? $location_settings['location_footer_menu_bg_color'] : $primaryColor);
			$menuPadding = ((!empty($location_settings['location_footer_menu_padding'])) ? $location_settings['location_footer_menu_padding'] : "15px 0");
			$menuMargin = ((!empty($location_settings['location_footer_menu_margin'])) ? $location_settings['location_footer_menu_margin'] : "35px auto 0");
			$menuWidth = ((!empty($location_settings['location_footer_menu_width'])) ? $location_settings['location_footer_menu_width'] : "200px");
			$menuBorderRadius = ((!empty($location_settings['location_footer_menu_border_radius'])) ? $location_settings['location_footer_menu_border_radius'] : "0");
			$menuTxtColor = ((!empty($location_settings['location_footer_menu_txt_color'])) ? $location_settings['location_footer_menu_txt_color'] : $secondaryColor);
			$menuBorder = ((!empty($location_settings['location_footer_menu_border'])) ? $location_settings['location_footer_menu_border'] : "0");
			$menuTxtColorHover = ((!empty($location_settings['location_footer_menu_txt_color_hover'])) ? $location_settings['location_footer_menu_txt_color_hover'] : $secondaryColor);
			$menuBgColorHover = ((!empty($location_settings['location_footer_menu_bg_color_hover'])) ? $location_settings['location_footer_menu_bg_color_hover'] : $secondaryColor);
			$menuMobileMargin = ((!empty($location_settings['location_footer_menu_mobile_margin'])) ? $location_settings['location_footer_menu_mobile_margin'] : $menuMargin);
			$menuMobileWidth = ((!empty($location_settings['location_footer_menu_mobile_width'])) ? $location_settings['location_footer_menu_mobile_width'] : $menuWidth);
			$menuMobilePadding = ((!empty($location_settings['location_footer_menu_mobile_padding'])) ? $location_settings['location_footer_menu_mobile_padding'] : $menuPadding);
			$menuBreakPoint = ((!empty($location_settings['location_footer_mobile_breakpoint'])) ? $location_settings['location_footer_mobile_breakpoint'] : "800px");
			?>section.location-without-maps .wrapper-container ul.view-more {
				background-color: <?php echo $menuBgColor; ?>;
				margin: <?php echo $menuMargin; ?>;
				width: <?php echo $menuWidth; ?>;
				border-radius: <?php echo $menuBorderRadius; ?>;
				color: <?php echo $menuTxtColor; ?>;
				border: <?php echo $menuBorder; ?>;
				transition: .3s;
			}

			section.location-without-maps .wrapper-container ul.view-more li {
				padding: <?php echo $menuPadding; ?>;
			}

			section.location-without-maps .wrapper-container ul.view-more:hover {
				background-color: <?php echo $menuBgColorHover; ?>;
				color: <?php echo $menuTxtColorHover; ?> transition: .3s;
			}

			@media(max-width: <?php echo $menuBreakPoint; ?>) {
				section.location-without-maps .wrapper-container ul.view-more {
					width: <?php echo $menuMobileWidth; ?>;
					margin: <?php echo $menuMobileMargin; ?>;
				}

				section.location-without-maps .wrapper-container ul.view-more li {
					padding: <?php echo $menuMobilePadding; ?>;
				}
			}

			/* Footer View More SubMenu */
			<?php
			$submenuBgColor = ((!empty($location_settings['location_footer_submenu_bg_color'])) ? $location_settings['location_footer_submenu_bg_color'] : $primaryColor);
			$submenuPadding = ((!empty($location_settings['location_footer_submenu_padding'])) ? $location_settings['location_footer_submenu_padding'] : "15px 0");
			$submenuMargin = ((!empty($location_settings['location_footer_submenu_margin'])) ? $location_settings['location_footer_submenu_margin'] : "0");
			$submenuWidth = ((!empty($location_settings['location_footer_submenu_width'])) ? $location_settings['location_footer_submenu_width'] : "100%");
			$submenuBorderRadius = ((!empty($location_settings['location_footer_submenu_border_radius'])) ? $location_settings['location_footer_submenu_border_radius'] : "0");
			$submenuTxtColor = ((!empty($location_settings['location_footer_submenu_txt_color'])) ? $location_settings['location_footer_submenu_txt_color'] : $secondaryColor);
			$submenuBorder = ((!empty($location_settings['location_footer_submenu_border'])) ? $location_settings['location_footer_submenu_border'] : "0");
			$submenuTxtColorHover = ((!empty($location_settings['location_footer_submenu_txt_color_hover'])) ? $location_settings['location_footer_submenu_txt_color_hover'] : $secondaryColor);
			$submenuBgColorHover = ((!empty($location_settings['location_footer_submenu_bg_color_hover'])) ? $location_settings['location_footer_submenu_bg_color_hover'] : $secondaryColor);
			?>section.location-without-maps .wrapper-container ul.view-more li ul {
				background-color: <?php echo $submenuBgColor; ?>;
				margin: <?php echo $submenuMargin; ?>;
				width: <?php echo $submenuWidth; ?>;
				border-radius: <?php echo $submenuBorderRadius; ?>;
				border: <?php echo $submenuBorder; ?>;
				transition: .3s;
			}

			section.location-without-maps .wrapper-container ul.view-more li ul a {
				color: <?php echo $submenuTxtColor; ?>;
			}

			section.location-without-maps .wrapper-container ul.view-more li ul li {
				padding: <?php echo $submenuPadding; ?>;
			}

			section.location-without-maps .wrapper-container ul.view-more li ul a:hover {
				color: <?php echo $submenuTxtColorHover; ?>
			}

			section.location-without-maps .wrapper-container ul.view-more li ul a:hover li {
				background-color: <?php echo $submenuBgColorHover; ?>;
				transition: .3s;
			}

			<?php if (!empty($location_settings['location_footer_primary_hover'])) : ?>.locations .location-without-maps a:hover .footer-primary-color {
				color: <?php echo $location_settings['location_footer_primary_hover']; ?>;
				transition: .3s;
			}

			<?php endif; ?><?php if (!empty($location_settings['location_footer_secondary_txt'])) : ?>.locations .location-without-maps .footer-secondary-color {
				color: <?php echo $location_settings['location_footer_secondary_txt']; ?>;
			}

			<?php endif; ?>

			/*
			 * Multi Location maps Settings style
			 */
			<?php if (!empty($location_settings['location_bg'])) : ?>section.location {
				background-color: <?php echo $location_settings['location_bg']; ?>;
			}

			<?php else : ?><?php if (!empty($general_settings['primary_color'])) : ?>section.location {
				background-color: <?php echo $general_settings['primary_color']; ?>;
			}

			<?php endif; ?><?php endif; ?><?php if (!empty($location_settings['location_text_color'])) : ?>section.location .location-wrapper .map-locations ul li a {
				color: <?php echo $location_settings['location_text_color']; ?>;
			}

			<?php else : ?><?php if (!empty($general_settings['secondary_color'])) : ?>section.location .location-wrapper .map-locations ul li a {
				color: <?php echo $general_settings['secondary_color']; ?>;
			}

			<?php endif; ?><?php endif; ?><?php if (!empty($location_settings['location_text_hover_color'])) : ?>section.location .location-wrapper .map-locations ul li a:hover {
				color: <?php echo $location_settings['location_text_hover_color']; ?>;
			}

			<?php else : ?><?php if (!empty($general_settings['secondary_highlight_color'])) : ?>section.location .location-wrapper .map-locations ul li a:hover {
				color: <?php echo $general_settings['secondary_highlight_color']; ?>;
			}

			<?php endif; ?><?php endif; ?>

			/*
			 * single template left button style
			 */
			<?php if (!empty($location_settings['sl_left_btn_bg']) || !empty($location_settings['sl_left_btn_radius'])) : ?>.single-location-container a.button-secondary.location-button {
				background-color: <?php echo $location_settings['sl_left_btn_bg']; ?>;
				border-radius: <?php echo $location_settings['sl_left_btn_radius'] . "!important"; ?>;
			}

			<?php else : ?><?php if (!empty($general_settings['secondary_color'])) : ?>.single-location-container a.button-secondary.location-button {
				background-color: <?php echo $general_settings['secondary_color']; ?>;
			}

			<?php endif; ?><?php endif; ?><?php if (!empty($location_settings['sl_left_btn_hover_bg'])) : ?>.single-location-container a.button-secondary.location-button:hover {
				background-color: <?php echo $location_settings['sl_left_btn_hover_bg']; ?>;
			}

			<?php else : ?><?php if (!empty($general_settings['secondary_highlight_color'])) : ?>.single-location-container a.button-secondary.location-button:hover {
				background-color: <?php echo $general_settings['secondary_highlight_color']; ?>;
			}

			<?php endif; ?><?php endif; ?><?php if (!empty($location_settings['sl_left_btn_color'])) : ?>.single-location-container a.button-secondary.location-button {
				color: <?php echo $location_settings['sl_left_btn_color']; ?>;
			}

			<?php endif; ?><?php if (!empty($location_settings['sl_left_btn_hover_color'])) : ?>.single-location-container a.button-secondary.location-button:hover {
				color: <?php echo $location_settings['sl_left_btn_hover_color']; ?>;
			}

			<?php endif; ?>

			/*
			 * single template Right button style
			 */
			<?php if (!empty($location_settings['sl_right_btn_bg']) || !empty($location_settings['sl_left_btn_radius'])) : ?>.single-location-container a.button-primary.location-button {
				background-color: <?php echo $location_settings['sl_right_btn_bg']; ?>;
				border-radius: <?php echo $location_settings['sl_right_btn_radius']; ?>;
			}

			<?php else : ?><?php if (!empty($general_settings['primary_color'])) : ?>.single-location-container a.button-primary.location-button {
				background-color: <?php echo $general_settings['primary_color']; ?>;
			}

			<?php endif; ?><?php endif; ?><?php if (!empty($location_settings['sl_right_btn_hover_bg'])) : ?>.single-location-container a.button-primary.location-button:hover {
				background-color: <?php echo $location_settings['sl_right_btn_hover_bg']; ?>;
			}

			<?php else : ?><?php if (!empty($general_settings['primary_highlight_color'])) : ?>.single-location-container a.button-primary.location-button:hover {
				background-color: <?php echo $general_settings['primary_highlight_color']; ?>;
			}

			<?php endif; ?><?php endif; ?><?php if (!empty($location_settings['sl_right_btn_color'])) : ?>.single-location-container a.button-primary.location-button {
				color: <?php echo $location_settings['sl_right_btn_color']; ?>;
			}

			<?php endif; ?><?php if (!empty($location_settings['sl_right_btn_hover_color'])) : ?>.single-location-container a.button-primary.location-button:hover {
				color: <?php echo $location_settings['sl_right_btn_hover_color']; ?>;
			}

			<?php endif; ?>

			/*
			 * single template Review button style
			 */
			<?php if (!empty($location_settings['sl_review_btn_bg']) || !empty($location_settings['sl_left_btn_radius'])) : ?>.single-location-container a.button-primary.location-button.review-btn {
				<?php if (in_array('sl_review_btn_bg', $location_settings)) : ?>background-color: <?php echo $location_settings['sl_review_btn_bg']; ?>;
				<?php endif; ?><?php if (in_array('sl_left_btn_radius', $location_settings)) : ?>border-radius: <?php echo $location_settings['sl_review_btn_radius']; ?>;
				<?php endif; ?>
			}

			<?php else : ?><?php if (!empty($general_settings['primary_color'])) : ?>.single-location-container a.button-primary.location-button.review-btn {
				background-color: <?php echo $general_settings['primary_color']; ?>;
			}

			<?php endif; ?><?php endif; ?><?php if (!empty($location_settings['sl_review_btn_hover_bg'])) : ?>.single-location-container a.button-primary.location-button.review-btn:hover {
				background-color: <?php echo $location_settings['sl_review_btn_hover_bg']; ?>;
			}

			<?php else : ?><?php if (!empty($general_settings['primary_highlight_color'])) : ?>.single-location-container a.button-primary.location-button.review-btn:hover {
				background-color: <?php echo $general_settings['primary_highlight_color']; ?>;
			}

			<?php endif; ?><?php endif; ?><?php if (!empty($location_settings['sl_review_btn_color'])) : ?>.single-location-container a.button-primary.location-button.review-btn {
				color: <?php echo $location_settings['sl_review_btn_color']; ?>;
			}

			<?php endif; ?><?php if (!empty($location_settings['sl_review_btn_hover_color'])) : ?>.single-location-container a.button-primary.location-button.review-btn:hover {
				color: <?php echo $location_settings['sl_review_btn_hover_color']; ?>;
			}

			<?php endif; ?>
		</style>
<?php
	}
}
