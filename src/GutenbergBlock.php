<?php
namespace Hip\Location;

use Carbon_Fields\Block;
use Carbon_Fields\Field;

class GutenbergBlock
{
	protected $pluginDir;
	protected $post_type;
	public function __construct($args)
	{
		\Carbon_Fields\Carbon_Fields::boot();
		$this->init();
		$this->pluginDir = $args['plugin_dir'];
		$this->post_type = $args['post_type'];
	}
	public function init(){
		Block::make(__('Hip Location'))
			->add_fields(array(
				Field::make('separator', 'hip_location_block_layout', __('Hip Location Layout')),
				Field::make( 'radio_image', 'hip_location_layout', __( 'Choose Location Layout' ) )
					->set_options( array(
						'layout-1' => plugin_dir_url( __FILE__ ).'img/map-with-location.jpg',
						'layout-2' =>  plugin_dir_url( __FILE__ ).'img/locations.jpg',
					) )->set_default_value('layout-1')
					->set_classes('fullwidth-layout-options')
					->set_width(100),
				Field::make('separator', 'hip_location_block_title', __('Hip Location Settings')),
				Field::make('text', 'hip_location_block_name', __('Hip Location Heading')),
			))
			->set_render_callback(function ($fields, $attributes, $inner_blocks) {
				$locations = get_posts( ['post_type' => $this->post_type ] );
				if($fields['hip_location_layout'] === 'layout-1'):
					include $this->pluginDir .'/inc/multi-location-map.php';
				else:
					include $this->pluginDir .'/inc/multi-location-without-map.php';
				endif;
			});
	}
}
