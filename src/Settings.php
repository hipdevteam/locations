<?php

namespace Hip\Location;

class Settings
{
	protected static $optionKey = '_hip_location_settings';

	protected $options;

	public static function getSettings()
	{
		$saved = get_option(self::$optionKey, []);
		return $saved;
	}

	public static function saveSettings(array $settings)
	{
		$option = [];
		foreach ($settings as $key => $setting) {
			$option[$key] = sanitize_text_field($setting);
		}

		update_option(self::$optionKey, $option);
		flush_rewrite_rules();
    }

	public function getOptions()
	{
		if ($this->options) {
			return $this->options;
		}
		
		$this->options = Settings::getSettings();
		return $this->options;
	}

	public function saveOptions($options)
	{
		$this->options = $options;
		Settings::saveSettings($options);
	}
}
