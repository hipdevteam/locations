<?php

namespace Hip\Location;

class SettingsPage
{
	protected $slug = 'location_settings';
	protected $assetsUrl;
	protected $post_type;

	private $saved_settings;
	protected $map_type = [
		'Road Map',
		'Satellite',
		'Hybrid',
		'Terrain'
	];
	protected $map_skin = [
		'Silver',
		'Retro',
		'Dark',
		'Night',
		'Aubergine',
		'Aqua',
		'Classic-Blue',
		'Earth',
		'Magnesium'
	];

	public function __construct($args)
	{
		$this->assetsUrl = $args['plugin_url'] . '/assets';
		$this->post_type = $args['post_type'];
		$this->saved_settings = \Hip\Location\Settings::getSettings();;
	}

	public function addOptionsPage()
	{


		if (class_exists('Hip\Theme\Settings\General')) {
			add_submenu_page(
				'hip_settings',
				'Location Settings page',
				'Location Setting',
				'manage_options',
				$this->slug,
				[$this, 'optionsPageContent']
			);
		} else {
			add_submenu_page(
				'edit.php?post_type=location',
				'Location Settings',
				'Settings',
				'manage_options',
				$this->slug,
				[$this, 'optionsPageContent']
			);
		}
	}

	public function optionsPageContent()
	{
?>


		<div class="hip-location-setting">
			<h1>Location Maps Page Settings</h1>
			<br>
			<h2>CSS Syntax:</h2>
			<p>Padding: Top Right Bottom Left (i.e. 15px 0 0 0 sets top to 15px)&nbsp;&nbsp;<a href="https://www.w3schools.com/css/css_padding.asp" target="_blank">Click here for more info</a> </p>
			<p>Margin: Follows same pattern as padding&nbsp;&nbsp;<a href="https://www.w3schools.com/css/css_margin.asp" target="_blank">Click here for more info</a> </p>
			<p>Border: Thickness Pattern Color (i.e. 2px solid #FFF)&nbsp;&nbsp;<a href="https://www.w3schools.com/css/css_border.asp" target="_blank">Click here for more info</a> </p>

			<div class="tab">
				<button class="tablinks" onclick="openOptions(event, 'General')" id="defaultOpen">General</button>
				<button class="tablinks" onclick="openOptions(event, 'Single-Location')">Single Location</button>
				<button class="tablinks" onclick="openOptions(event, 'Footer')">Footer</button>
				<button class="tablinks" onclick="openOptions(event, 'Map')">Map</button>
			</div>
			<form id="location-settings-form">
				<div id="General" class="tabcontent">
					<h2>Location Settings</h2>
					<table class="form-table">
						<tbody>
							<tr>
								<th>Google API Key</th>
								<td>
									<input name="google_api_key" id="google_api_key" class="regular-text" type="text" />
								</td>
							</tr>
							<tr>
								<th>Background of location Map</th>
								<td>
									<input name="location_bg" id="location_bg" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Text Color of location Map</th>
								<td>
									<input name="location_text_color" id="location_text_color" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Text Hover Color of location Map</th>
								<td>
									<input name="location_text_hover_color" id="location_text_hover_color" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Include Archive? <span><br>This option is only available with Tabby Theme</span></th>
								<td>
									<label>Yes <input name="location_has_archive" id="location_has_archive_true" type="radio" value="true" /></label>&nbsp;&nbsp;
									<label>No <input name="location_has_archive" id="location_has_archive_false" type="radio" value="false" /></label>
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div id="Footer" class="tabcontent">
					<h2>Footer Settings</h2>
					<br>
					<table class="form-table">
						<tbody>
							<tr>
								<th>Location Header Title</th>
								<td>
									<input id="location_footer_title" name="location_footer_title" type="text" class="regular-text" />
								</td>
							</tr>
							<tr>
								<th>Single Location Title Text <span>(Default: Location's city)</span></th>
								<td>
									<input id="sl_footer_title_txt" name="sl_footer_title_txt" type="text" class="regular-text" />
								</td>
							</tr>
							<tr>
								<th>How many locations do you want per row?<span><br>(Default: 3)</span></th>
								<td>
									<input id="location_break_row_on" name="location_break_row_on" type="text" class="regular-text" /><span class="checkTheInput" style="color: red;"></span>
								</td>
							</tr>
							<tr>
								<th>Primary Text Color <br><span>(Default: Theme primary color)</span></th>
								<td>
									<input name="location_footer_primary_txt" id="location_footer_primary_txt" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Primary Text Hover Color</th>
								<td>
									<input name="location_footer_primary_hover" id="location_footer_primary_hover" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Secondary Text Color</th>
								<td>
									<input name="location_footer_secondary_txt" id="location_footer_secondary_txt" class="hip-colorpicker" type="text" />
								</td>
							</tr>
						</tbody>
					</table>
					<h3>Footer Mobile Call Button</h3>
					<table class="form-table">
						<tbody>
							<tr>
								<th>Button Background Color</th>
								<td>
									<input name="location_footer_button_bg_color" id="location_footer_button_bg_color" class="hip-colorpicker" type="text" />
								</td>
								<th>Button Background Color Hover</th>
								<td>
									<input name="location_footer_button_bg_color_hover" id="location_footer_button_bg_color_hover" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Button Text Color</th>
								<td>
									<input name="location_footer_button_txt_color" id="location_footer_button_txt_color" class="hip-colorpicker" type="text" />
								</td>
								<th>Button Text Color Hover</th>
								<td>
									<input name="location_footer_button_txt_color_hover" id="location_footer_button_txt_color_hover" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Button Padding</th>
								<td>
									<input name="location_footer_button_padding" id="location_footer_button_padding" class="regular-text" type="text" placeholder="Default: 15px " />
								</td>
								<th>Button Margin</th>
								<td>
									<input name="location_footer_button_margin" id="location_footer_button_margin" class="regular-text" type="text" placeholder="Default: 15px 0 25px " />
								</td>
							</tr>
							<tr>
								<th>Button Border Radius</th>
								<td>
									<input name="location_footer_button_border_radius" id="location_footer_button_border_radius" class="regular-text" type="text" placeholder="Default: 50px" />
								</td>
								<th>Button border</th>
								<td>
									<input name="location_footer_button_border" id="location_footer_button_border" class="regular-text" type="text" placeholder="Default: 0 " />
								</td>
							</tr>
							<tr>
								<th>Button bottom-border</th>
								<td>
									<input name="location_footer_button_bottom_border" id="location_footer_button_bottom_border" class="regular-text" type="text" placeholder="Default: 0 " />
								</td>
								<th>Button width</th>
								<td>
									<input name="location_footer_button_width" id="location_footer_button_width" class="regular-text" type="text" placeholder="Default: 200px" />
								</td>
							</tr>
						</tbody>
					</table>
					<h3>Footer Menu</h3>
					<table class="form-table">
						<tbody>
							<tr>
								<th>Menu Background Color</th>
								<td>
									<input name="location_footer_menu_bg_color" id="location_footer_menu_bg_color" class="hip-colorpicker" type="text" />
								</td>
								<th>Menu Background Color Hover</th>
								<td>
									<input name="location_footer_menu_bg_color_hover" id="location_footer_menu_bg_color_hover" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Menu Text Color</th>
								<td>
									<input name="location_footer_menu_txt_color" id="location_footer_menu_txt_color" class="hip-colorpicker" type="text" />
								</td>
								<th>Menu Text Color Hover</th>
								<td>
									<input name="location_footer_menu_txt_color_hover" id="location_footer_menu_txt_color_hover" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Menu Padding</th>
								<td>
									<input name="location_footer_menu_padding" id="location_footer_menu_padding" class="regular-text" type="text" placeholder="Default: 15px 0  " />
								</td>
								<th>Menu Mobile Padding</th>
								<td>
									<input name="location_footer_menu_mobile_padding" id="location_footer_menu_mobile_padding" class="regular-text" type="text" placeholder="Default: Inherits Menu Padding" />
								</td>
							</tr>
							<tr>
								<th>Menu Margin</th>
								<td>
									<input name="location_footer_menu_margin" id="location_footer_menu_margin" class="regular-text" type="text" placeholder="Default: 35px auto 0 " />
								</td>
								<th>Menu Mobile Margin</th>
								<td>
									<input name="location_footer_menu_mobile_margin" id="location_footer_menu_mobile_margin" class="regular-text" type="text" placeholder="Default: Inherits Menu Margin" />
								</td>
							</tr>
							<tr>
								<th>Menu Border Radius</th>
								<td>
									<input name="location_footer_menu_border_radius" id="location_footer_menu_border_radius" class="regular-text" type="text" placeholder="Default: 0px" />
								</td>
								<th>Menu Border</th>
								<td>
									<input name="location_footer_menu_border" id="location_footer_menu_border" class="regular-text" type="text" placeholder="Default: 0 " />
								</td>
							</tr>
							<tr>
								<th>Menu Width</th>
								<td>
									<input name="location_footer_menu_width" id="location_footer_menu_width" class="regular-text" type="text" placeholder="Default: 200px" />
								</td>
								<th>Menu Mobile Width</th>
								<td>
									<input name="location_footer_menu_mobile_width" id="location_footer_menu_mobile_width" class="regular-text" type="text" placeholder="Default: Inherits Menu width" />
								</td>
							</tr>
							<tr>
								<th>Menu Mobile Beakpoint</th>
								<td>
									<input name="location_footer_mobile_breakpoint" id="location_footer_mobile_breakpoint" placeholder="Default: 800px" class="regular-text" type="text" />
								</td>
							</tr>
						</tbody>
					</table>
					<h3>Footer Submenu</h3>
					<table class="form-table">
						<tbody>
							<tr>
								<th>Submenu Background Color</th>
								<td>
									<input name="location_footer_submenu_bg_color" id="location_footer_submenu_bg_color" class="hip-colorpicker" type="text" />
								</td>
								<th>Submenu Background Color Hover</th>
								<td>
									<input name="location_footer_submenu_bg_color_hover" id="location_footer_submenu_bg_color_hover" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Submenu Text Color</th>
								<td>
									<input name="location_footer_submenu_txt_color" id="location_footer_submenu_txt_color" class="hip-colorpicker" type="text" />
								</td>
								<th>Submenu Text Color Hover</th>
								<td>
									<input name="location_footer_submenu_txt_color_hover" id="location_footer_submenu_txt_color_hover" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Submenu Padding</th>
								<td>
									<input name="location_footer_submenu_padding" id="location_footer_submenu_padding" class="regular-text" type="text" placeholder="Default: 15px 0 " />
								</td>
								<th>Submenu Margin</th>
								<td>
									<input name="location_footer_submenu_margin" id="location_footer_submenu_margin" class="regular-text" type="text" placeholder="Default: 0 " />
								</td>
							</tr>
							<tr>
								<th>Submenu Border Radius</th>
								<td>
									<input name="location_footer_submenu_border_radius" id="location_footer_submenu_border_radius" class="regular-text" type="text" placeholder="Default: 0" />
								</td>
								<th>Submenu Border</th>
								<td>
									<input name="location_footer_submenu_border" id="location_footer_submenu_border" class="regular-text" type="text" placeholder="Default: 0 " />
								</td>
							</tr>
							<tr>
								<th>Submenu Width</th>
								<td>
									<input name="location_footer_submenu_width" id="location_footer_submenu_width" class="regular-text" type="text" placeholder="Default: 100%" />
								</td>
							</tr>
						</tbody>
					</table>
				</div>

				<div id="Single-Location" class="tabcontent">
					<h2>Single Location Template Settings</h2>
					<br>
					<h2>Left Button</h2>
					<table class="form-table">
						<tbody>
							<tr>
								<th>Button Text</th>
								<td>
									<input id="sl_left_btn_text" name="sl_left_btn_text" type="text" class="regular-text" />
								</td>
							</tr>
							<tr>
								<th>Button Link</th>
								<td>
									<input id="sl_left_btn_link" name="sl_left_btn_link" type="text" class="regular-text" />
								</td>
							</tr>
						</tbody>

						<tbody>
							<tr>
								<th>Button Background</th>
								<td>
									<input name="sl_left_btn_bg" id="sl_left_btn_bg" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Button Text Color</th>
								<td>
									<input name="sl_left_btn_color" id="sl_left_btn_color" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Button Hover Background</th>
								<td>
									<input name="sl_left_btn_hover_bg" id="sl_left_btn_hover_bg" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Button Text Hover Color</th>
								<td>
									<input name="sl_left_btn_hover_color" id="sl_left_btn_hover_color" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Button Radius</th>
								<td>
									<input name="sl_left_btn_radius" id="sl_left_btn_radius" class="regular-text" type="text" />
								</td>
							</tr>
						</tbody>

					</table>
					<br>

					<h2>Right Button</h2>
					<table class="form-table">
						<tbody>
							<tr>
								<th>Button Text</th>
								<td>
									<input id="sl_right_btn_text" name="sl_right_btn_text" type="text" class="regular-text" />
								</td>
							</tr>
							<tr>
								<th>Button Link</th>
								<td>
									<input id="sl_right_btn_link" name="sl_right_btn_link" type="text" class="regular-text" />
								</td>
							</tr>
						</tbody>

						<tbody>
							<tr>
								<th>Button Background</th>
								<td>
									<input name="sl_right_btn_bg" id="sl_right_btn_bg" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Button Text Color</th>
								<td>
									<input name="sl_right_btn_color" id="sl_right_btn_color" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Button Hover Background</th>
								<td>
									<input name="sl_right_btn_hover_bg" id="sl_right_btn_hover_bg" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Button Text Hover Color</th>
								<td>
									<input name="sl_right_btn_hover_color" id="sl_right_btn_hover_color" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Button Radius</th>
								<td>
									<input name="sl_right_btn_radius" id="sl_right_btn_radius" class="regular-text" type="text" />
								</td>
							</tr>
						</tbody>

					</table>


					<h2>Review Button</h2>
					<table class="form-table">
						<tbody>
							<tr>
								<th>Button Text</th>
								<td>
									<input id="sl_review_btn_text" name="sl_review_btn_text" type="text" class="regular-text" />
								</td>
							</tr>
						</tbody>

						<tbody>
							<tr>
								<th>Button Background</th>
								<td>
									<input name="sl_review_btn_bg" id="sl_review_btn_bg" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Button Text Color</th>
								<td>
									<input name="sl_review_btn_color" id="sl_review_btn_color" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Button Hover Background</th>
								<td>
									<input name="sl_review_btn_hover_bg" id="sl_review_btn_hover_bg" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Button Text Hover Color</th>
								<td>
									<input name="sl_review_btn_hover_color" id="sl_review_btn_hover_color" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Button Radius</th>
								<td>
									<input name="sl_review_btn_radius" id="sl_review_btn_radius" class="regular-text" type="text" />
								</td>
							</tr>
						</tbody>

					</table>


				</div>


				<div id="Map" class="tabcontent">
					<h2>Map Center and Zoom</h2>
					<table class="form-table">
						<tr>
							<th><?php _e('Center Latitude', 'location') ?></th>
							<td>
								<input id="map_latitude" name="map_latitude" type="text" class="regular-text" />
							</td>
						</tr>
						<tr>
							<th><?php _e('Center Longitude', 'location') ?></th>
							<td>
								<input id="map_longitude" name="map_longitude" type="text" class="regular-text" />
							</td>
						</tr>
						<tr>
							<th><?php _e('Zoom', 'location') ?></th>
							<td>
								<input id="map_zoom" name="map_zoom" type="number" />
							</td>
						</tr>
						<tr>
							<th>Map Type</th>
							<td>
								<?php $selected = !empty($this->saved_settings['map_type']) ? $this->saved_settings['map_type'] : ''; ?>
								<select id="map_type" name="map_type">
									<?php foreach ($this->map_type as $type) {
										$select = ($selected == $type) ? ' selected' : '';
										echo "<option value='$type' $select>$type</option>";
									} ?>
								</select>
							</td>
						</tr>
						<tr>
							<th>Map Skin</th>
							<td>
								<?php $selected = !empty($this->saved_settings['map_skin']) ? $this->saved_settings['map_skin'] : ''; ?>
								<select id="map_skin" name="map_skin">
									<?php foreach ($this->map_skin as $skin) {
										$select = ($selected == $skin) ? ' selected' : '';
										echo "<option value='$skin' $select>$skin</option>";
									} ?>
								</select>
							</td>

						</tr>
					</table>
					<h2>Customize Map Pin and Hover Bubble</h2>
					<table class="form-table">
						<tbody>
							<tr class="upload_map_pin">
								<th><?php _e('Upload Map Banner Image', 'location') ?></th>
								<td>
									<img src="." alt="" class="map_pin_banner_img banner" width="100" height="100">
									<button type="button" name="upload_map_pin_banner" class="upload_map_pin_banner button"><?php echo !empty($this->saved_settings['map_pin_banner']) ? 'Change' : 'Upload' ?> Map Banner</button>
									<input type="hidden" value="" name="map_pin_banner" id="map_pin_banner">
									<?php if (!empty($this->saved_settings['map_pin_banner'])) : ?>
										<button type="button" name="remove_map_pin_banner" class="remove_map_pin_banner button">Remove Map Banner</button>
									<?php endif; ?>
								</td>
							</tr>
							<tr class="upload_map_pin">
								<th><?php _e('Upload Map Pin Image', 'location') ?></th>
								<td>
									<img src="." alt="" class="map_pin_icon logo" width="100" height="100">
									<button type="button" name="upload_map_pin_icon" class="upload_map_pin_icon button"><?php echo !empty($this->saved_settings['map_pin_img']) ? 'Change' : 'Upload' ?> Map Pin</button>
									<input type="hidden" value="" name="map_pin_img" id="map_pin_img">
									<?php if (!empty($this->saved_settings['map_pin_img'])) : ?>
										<button type="button" name="remove_map_pin_icon" class="remove_map_pin_icon button">Remove Map Pin</button>
									<?php endif; ?>
								</td>
							</tr>
							<tr>
								<th>Map Pin Icon Width</th>
								<td>
									<input id="map_pin_icon_size_width" name="map_pin_icon_size_width" type="text" class="regular-text" />
								</td>
							</tr>
							<tr>
								<th>Map Pin Icon Height</th>
								<td>
									<input id="map_pin_icon_size_height" name="map_pin_icon_size_height" type="text" class="regular-text" />
								</td>
							</tr>

						</tbody>

						<tbody>
							<tr>
								<th>Bubble Info Background</th>
								<td>
									<input name="bubble_info_bg" id="bubble_info_bg" class="hip-colorpicker" type="text" />
								</td>
							</tr>
							<tr>
								<th>Bubble Text Color</th>
								<td>
									<input name="bubble_info_color" id="bubble_info_color" class="hip-colorpicker" type="text" />
								</td>
							</tr>
						</tbody>

					</table>
				</div>
				<button type="submit" class="button-primary location-submit">Submit</button>
				<span id="feedback"></span>
			</form>
		</div>
		<script>
			// Get the element with id="defaultOpen" and click on it
			document.getElementById("defaultOpen").click();

			document.querySelector('.location-submit').addEventListener("click", function(event) {
				var checkInput = document.querySelector('#location_break_row_on');
				if (isNaN(checkInput.value)) {
					event.preventDefault();
					checkInput.style.border = "1px solid red";
					document.querySelector('.checkTheInput').innerHTML = "<br>This must be a number";
					document.getElementById('location-settings-form').scrollIntoView();
				} else {
					checkInput.style.border = "0";
					document.querySelector('.checkTheInput').innerHTML = "";
				}
			})
		</script>
<?php
	}

	public function enqueueAssets()
	{
		if (strpos(get_current_screen()->id, $this->slug)) {
			wp_enqueue_style($this->slug . '-styles', $this->assetsUrl . '/css/admin-setting.css');
			wp_enqueue_style('wp-color-picker');
			wp_register_script($this->slug,  $this->assetsUrl . '/js/admin.js', ['jquery', 'wp-color-picker']);
			wp_localize_script($this->slug, 'hiplocation', [
				'strings' => [
					'saved' => __('Settings Saved', 'hip'),
					'error' => __('Error', 'hip')
				],
				'api'     => [
					'url'   => esc_url_raw(rest_url('hip-api/v1/location/settings')),
					'nonce' => wp_create_nonce('wp_rest')
				]
			]);
			wp_enqueue_script($this->slug);
		}
	}
}
