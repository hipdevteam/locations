<?php

namespace Hip\Location;

class Frontend
{
	public function enqueueScriptsAndStyles($plugin_url, $version)
	{

		wp_enqueue_style(
			'maps-location-style',
			$plugin_url  . '/assets/css/front.css',
			[],
			$version
		);

		wp_enqueue_script(
			'maps-location-js',
			$plugin_url . '/assets/js/front.js',
			['jquery'],
			$version
		);
	}

	public function googleMapsJS()
	{
		$location_settings = \Hip\Location\Settings::getSettings();

		if (!empty($location_settings['google_api_key'])) {
			$api = sprintf('https://maps.googleapis.com/maps/api/js?key=%1$s&callback&libraries=places', $location_settings['google_api_key']);

			?>
				 <script>

						 // Create the script tag, set the appropriate attributes
						 var script = document.createElement('script');
						 script.src ="<?php echo $api ?>";
						 script.async = true;
						 // Append the 'script' element to 'head'
						 if (document.querySelector('script[src^="https://maps.googleapis.com/maps/api/js?"]') === null) {
							 document.head.appendChild(script);
						 }

            </script>
			<?php
		}
	}

	public function renderMultiLocationMap($post_type, $plugin_dir)
	{
		$locations = get_posts(['numberposts' => -1, 'post_type' => $post_type]);
		ob_start();
		include $plugin_dir . '/inc/multi-location-map.php';
		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}

	public function renderMultiLocationWithoutMap($post_type, $plugin_dir)
	{
		$locations = get_posts(['numberposts' => -1, 'post_type' => $post_type, 'orderby' => 'menu_order', 'order' => 'ASC']);
		ob_start();
		include $plugin_dir . '/inc/multi-location-without-map.php';
		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}

	public function singleTemplate($template, $post_type, $plugin_dir)
	{
		global $post;
		if ($post->post_type == $post_type) {
			$template = $plugin_dir . '/inc/single.php';
		}

		return $template;
	}

	public function renderSingleMapOnly($post_type, $plugin_dir, $slug, $zoom_attr, $height_attr) {
		$location = get_posts(['numberposts' => 1, 'post_type' => $post_type, 'name' => $slug])[0];
		$settings = \Hip\Location\Settings::getSettings();
		$test = get_post_meta($location);
		$zoom = $zoom_attr ? $zoom_attr : 15;
		$height = $height_attr ? $height_attr : '360px';

		ob_start();
		include $plugin_dir . '/inc/single-location-map.php';
		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}

	public function renderMapOnly($post_type, $plugin_dir)
	{
		$locations = get_posts(['numberposts' => -1, 'post_type' => $post_type]);
		$settings = \Hip\Location\Settings::getSettings();
		$lat = $settings['map_latitude'];
		$long = $settings['map_longitude'];
		$zoom = $settings['map_zoom'];

		ob_start();
		include $plugin_dir . '/inc/map-only.php';
		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}

	public function renderSingleLocationMapOnly( $plugin_dir)
	{
		ob_start();
		global $post;
		$current_post_id = $post->ID;
		$place_id = get_post_meta( $current_post_id, 'location_place_id', true );
		include $plugin_dir . '/inc/single-location-maponly.php';
		$html = ob_get_contents();
		ob_end_clean();

		return $html;
	}

	public function needDropdown($locations)
	{
		foreach ($locations as $index => $location) {
			if (get_post_meta($location->ID, 'location_show_in_footer', true) === "do_not_show_in_footer") return true;
		}
		return false;
	}

	public function checkPrimaryTxtColor()
	{
		if (class_exists('Hip\Location\Settings')) {
			$location_settings = \Hip\Location\Settings::getSettings();
		}
		if (!empty($location_settings['location_footer_primary_txt'])) {
			echo "footer-primary-color";
		} else {
			echo "primary-txt";
		}
	}

	public function getTitle($city, $state, $stateOption)
	{
		if (class_exists('Hip\Location\Settings')) {
			$location_settings = \Hip\Location\Settings::getSettings();
		}
		if (!empty($location_settings['sl_footer_title_txt'])) {
			return $location_settings['sl_footer_title_txt'];
		} else {
			if ($stateOption == 'show_state') return "$city, $state";
			return $city;
		}
	}

	public function getPhoneNumber($phone)
	{
		global $tabbyFields;
		if (isset($tabbyFields)) {
			return ($tabbyFields['tabby_callrail_avoidance'] == 1) ? implode('<span></span>', str_split($phone)) : $phone;
		} else {
			return $phone;
		}
	}
}
