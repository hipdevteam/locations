jQuery(function ($) {
	'use strict';
	if ($('.hip-location-setting').length) {
		$('.hip-colorpicker').wpColorPicker();

		$.ajax({
			method: 'GET',
			url: hiplocation.api.url,
			dataType: 'json',
			beforeSend: function (xhr) {
				xhr.setRequestHeader('X-WP-Nonce', hiplocation.api.nonce);
			}
		}).then(function (r) {
			if (r.hasOwnProperty('google_api_key')) {
				$('#google_api_key').val(r.google_api_key);
			}
			if (r.hasOwnProperty('location_bg')) {
				$('#location_bg').wpColorPicker('color', r.location_bg);
			}
			if (r.hasOwnProperty('location_text_color')) {
				$('#location_text_color').wpColorPicker('color', r.location_text_color);
			}
			if (r.hasOwnProperty('location_text_hover_color')) {
				$('#location_text_hover_color').wpColorPicker('color', r.location_text_hover_color);
			}
			if (r.hasOwnProperty('location_break_row_on')) {
				$('#location_break_row_on').val(r.location_break_row_on);
			}
			if (r.hasOwnProperty('sl_left_btn_text')) {
				$('#sl_left_btn_text').val(r.sl_left_btn_text);
			}
			if (r.hasOwnProperty('sl_left_btn_link')) {
				$('#sl_left_btn_link').val(r.sl_left_btn_link);
			}
			if (r.hasOwnProperty('sl_left_btn_bg')) {
				$('#sl_left_btn_bg').wpColorPicker('color', r.sl_left_btn_bg);
			}
			if (r.hasOwnProperty('sl_left_btn_color')) {
				$('#sl_left_btn_color').wpColorPicker('color', r.sl_left_btn_color);
			}
			if (r.hasOwnProperty('sl_left_btn_hover_bg')) {
				$('#sl_left_btn_hover_bg').wpColorPicker('color', r.sl_left_btn_hover_bg);
			}
			if (r.hasOwnProperty('sl_left_btn_hover_color')) {
				$('#sl_left_btn_hover_color').wpColorPicker('color', r.sl_left_btn_hover_color);
			}
			if (r.hasOwnProperty('sl_left_btn_radius')) {
				$('#sl_left_btn_radius').val(r.sl_left_btn_radius);
			}
			if (r.hasOwnProperty('sl_right_btn_text')) {
				$('#sl_right_btn_text').val(r.sl_right_btn_text);
			}
			if (r.hasOwnProperty('sl_right_btn_link')) {
				$('#sl_right_btn_link').val(r.sl_right_btn_link);
			}
			if (r.hasOwnProperty('sl_right_btn_bg')) {
				$('#sl_right_btn_bg').wpColorPicker('color', r.sl_right_btn_bg);
			}
			if (r.hasOwnProperty('sl_right_btn_color')) {
				$('#sl_right_btn_color').wpColorPicker('color', r.sl_right_btn_color);
			}
			if (r.hasOwnProperty('sl_right_btn_hover_bg')) {
				$('#sl_right_btn_hover_bg').wpColorPicker('color', r.sl_right_btn_hover_bg);
			}
			if (r.hasOwnProperty('sl_right_btn_hover_color')) {
				$('#sl_right_btn_hover_color').wpColorPicker('color', r.sl_right_btn_hover_color);
			}
			if (r.hasOwnProperty('sl_right_btn_radius')) {
				$('#sl_right_btn_radius').val(r.sl_right_btn_radius);
			}
			if (r.hasOwnProperty('location_footer_title')) {
				$('#location_footer_title').val(r.location_footer_title);
			}
			if (r.hasOwnProperty('sl_footer_title_txt')) {
				$('#sl_footer_title_txt').val(r.sl_footer_title_txt);
			}
			if (r.hasOwnProperty('location_footer_primary_txt')) {
				$('#location_footer_primary_txt').wpColorPicker('color', r.location_footer_primary_txt);
			}
			if (r.hasOwnProperty('location_footer_primary_hover')) {
				$('#location_footer_primary_hover').wpColorPicker('color', r.location_footer_primary_hover);
			}
			if (r.hasOwnProperty('location_footer_secondary_txt')) {
				$('#location_footer_secondary_txt').wpColorPicker('color', r.location_footer_secondary_txt);
			}

			// Location Footer Call Button
			if (r.hasOwnProperty('location_footer_button_bg_color')) {
				$('#location_footer_button_bg_color').wpColorPicker('color', r.location_footer_button_bg_color);
			}
			if (r.hasOwnProperty('location_footer_button_txt_color')) {
				$('#location_footer_button_txt_color').wpColorPicker('color', r.location_footer_button_txt_color);
			}
			if (r.hasOwnProperty('location_footer_button_bg_color_hover')) {
				$('#location_footer_button_bg_color_hover').wpColorPicker('color', r.location_footer_button_bg_color_hover);
			}
			if (r.hasOwnProperty('location_footer_button_txt_color_hover')) {
				$('#location_footer_button_txt_color_hover').wpColorPicker('color', r.location_footer_button_txt_color_hover);
			}
			if (r.hasOwnProperty('location_footer_button_padding')) {
				$('#location_footer_button_padding').val(r.location_footer_button_padding);
			}
			if (r.hasOwnProperty('location_footer_button_margin')) {
				$('#location_footer_button_margin').val(r.location_footer_button_margin);
			}
			if (r.hasOwnProperty('location_footer_button_border')) {
				$('#location_footer_button_border').val(r.location_footer_button_border);
			}
			if (r.hasOwnProperty('location_footer_button_border_radius')) {
				$('#location_footer_button_border_radius').val(r.location_footer_button_border_radius);
			}
			if (r.hasOwnProperty('location_footer_button_width')) {
				$('#location_footer_button_width').val(r.location_footer_button_width);
			}
			if (r.hasOwnProperty('location_footer_button_bottom_border')) {
				$('#location_footer_button_bottom_border').val(r.location_footer_button_bottom_border);
			}
			// Location Footer View More menu
			if (r.hasOwnProperty('location_footer_menu_bg_color')) {
				$('#location_footer_menu_bg_color').wpColorPicker('color', r.location_footer_menu_bg_color);
			}
			if (r.hasOwnProperty('location_footer_menu_txt_color')) {
				$('#location_footer_menu_txt_color').wpColorPicker('color', r.location_footer_menu_txt_color);
			}
			if (r.hasOwnProperty('location_footer_menu_bg_color_hover')) {
				$('#location_footer_menu_bg_color_hover').wpColorPicker('color', r.location_footer_menu_bg_color_hover);
			}
			if (r.hasOwnProperty('location_footer_menu_txt_color_hover')) {
				$('#location_footer_menu_txt_color_hover').wpColorPicker('color', r.location_footer_menu_txt_color_hover);
			}
			if (r.hasOwnProperty('location_footer_menu_padding')) {
				$('#location_footer_menu_padding').val(r.location_footer_menu_padding);
			}
			if (r.hasOwnProperty('location_footer_menu_margin')) {
				$('#location_footer_menu_margin').val(r.location_footer_menu_margin);
			}
			if (r.hasOwnProperty('location_footer_menu_mobile_padding')) {
				$('#location_footer_menu_mobile_padding').val(r.location_footer_menu_mobile_padding);
			}
			if (r.hasOwnProperty('location_footer_menu_mobile_margin')) {
				$('#location_footer_menu_mobile_margin').val(r.location_footer_menu_mobile_margin);
			}
			if (r.hasOwnProperty('location_footer_menu_border')) {
				$('#location_footer_menu_border').val(r.location_footer_menu_border);
			}
			if (r.hasOwnProperty('location_footer_menu_border_radius')) {
				$('#location_footer_menu_border_radius').val(r.location_footer_menu_border_radius);
			}
			if (r.hasOwnProperty('location_footer_menu_width')) {
				$('#location_footer_menu_width').val(r.location_footer_menu_width);
			}
			if (r.hasOwnProperty('location_footer_menu_mobile_width')) {
				$('#location_footer_menu_mobile_width').val(r.location_footer_menu_mobile_width);
			}
			if (r.hasOwnProperty('location_footer_mobile_breakpoint')) {
				$('#location_footer_mobile_breakpoint').val(r.location_footer_mobile_breakpoint);
			}
			// Location Footer View More submenu
			if (r.hasOwnProperty('location_footer_submenu_bg_color')) {
				$('#location_footer_submenu_bg_color').wpColorPicker('color', r.location_footer_submenu_bg_color);
			}
			if (r.hasOwnProperty('location_footer_submenu_txt_color')) {
				$('#location_footer_submenu_txt_color').wpColorPicker('color', r.location_footer_submenu_txt_color);
			}
			if (r.hasOwnProperty('location_footer_submenu_bg_color_hover')) {
				$('#location_footer_submenu_bg_color_hover').wpColorPicker('color', r.location_footer_submenu_bg_color_hover);
			}
			if (r.hasOwnProperty('location_footer_submenu_txt_color_hover')) {
				$('#location_footer_submenu_txt_color_hover').wpColorPicker('color', r.location_footer_submenu_txt_color_hover);
			}
			if (r.hasOwnProperty('location_footer_submenu_padding')) {
				$('#location_footer_submenu_padding').val(r.location_footer_submenu_padding);
			}
			if (r.hasOwnProperty('location_footer_submenu_margin')) {
				$('#location_footer_submenu_margin').val(r.location_footer_submenu_margin);
			}
			if (r.hasOwnProperty('location_footer_submenu_border')) {
				$('#location_footer_submenu_border').val(r.location_footer_submenu_border);
			}
			if (r.hasOwnProperty('location_footer_submenu_border_radius')) {
				$('#location_footer_submenu_border_radius').val(r.location_footer_submenu_border_radius);
			}
			if (r.hasOwnProperty('location_footer_submenu_width')) {
				$('#location_footer_submenu_width').val(r.location_footer_submenu_width);
			}
			if (r.hasOwnProperty('sl_review_btn_text')) {
				$('#sl_review_btn_text').val(r.sl_review_btn_text);
			}
			if (r.hasOwnProperty('sl_review_btn_bg')) {
				$('#sl_review_btn_bg').wpColorPicker('color', r.sl_review_btn_bg);
			}
			if (r.hasOwnProperty('sl_review_btn_color')) {
				$('#sl_review_btn_color').wpColorPicker('color', r.sl_review_btn_color);
			}
			if (r.hasOwnProperty('sl_review_btn_hover_bg')) {
				$('#sl_review_btn_hover_bg').wpColorPicker('color', r.sl_review_btn_hover_bg);
			}
			if (r.hasOwnProperty('sl_review_btn_hover_color')) {
				$('#sl_review_btn_hover_color').wpColorPicker('color', r.sl_review_btn_hover_color);
			}
			if (r.hasOwnProperty('sl_review_btn_radius')) {
				$('#sl_review_btn_radius').val(r.sl_review_btn_radius);
			}
			// Uncomment if we want to add in archive slug option
			// if (r['location_archive_slug'] !== "") {
			//     console.log("True");
			//     $('#location_archive_slug').val(r.location_archive_slug);
			// } else {
			//     console.log("False");
			//     $('#location_archive_slug').val("location");
			// }
			if (r['location_has_archive'] === "true") {
				$('#location_has_archive_true').prop("checked", true);;
			} else if (r['location_has_archive'] === "false") {
				$('#location_has_archive_false').prop("checked", true);
			} else {
				$('#location_has_archive_false').prop("checked", true);
			}
			if (r.hasOwnProperty('map_type')) {
				$('#map_type').val(r.map_type);
			}
			if (r.hasOwnProperty('map_skin')) {
				$('#map_skin').val(r.map_skin);
			}

			for (var key in r) {
				if (r.hasOwnProperty(key)) {
					switch (key) {
						case 'map_pin_img':
							$('.map_pin_icon.logo').attr('src', r[key]);
							$('#map_pin_img').val(r[key]);
							break;
						case 'map_pin_banner':
							$('.map_pin_banner_img.banner').attr('src', r[key]);
							$('#map_pin_banner').val(r[key]);
							break;
					}
				}
			}

			if (r.hasOwnProperty('map_latitude')) {
				$('#map_latitude').val(r.map_latitude);
			}

			if (r.hasOwnProperty('map_longitude')) {
				$('#map_longitude').val(r.map_longitude);
			}

			if (r.hasOwnProperty('map_zoom')) {
				$('#map_zoom').val(r.map_zoom);
			}
			if (r.hasOwnProperty('map_pin_icon_size_width')) {
				$('#map_pin_icon_size_width').val(r.map_pin_icon_size_width);
			}
			if (r.hasOwnProperty('map_pin_icon_size_height')) {
				$('#map_pin_icon_size_height').val(r.map_pin_icon_size_height);
			}
			if (r.hasOwnProperty('bubble_info_bg')) {
				$('#bubble_info_bg').wpColorPicker('color', r.bubble_info_bg);
			}
			if (r.hasOwnProperty('bubble_info_color')) {
				$('#bubble_info_color').wpColorPicker('color', r.bubble_info_color);
			}
		});

		$('#location-settings-form').on('submit', function (e) {
			e.preventDefault();
			var data = $('#location-settings-form').serializeArray();

			$.ajax({
				method: 'POST',
				url: hiplocation.api.url,
				beforeSend: function (xhr) {
					xhr.setRequestHeader('X-WP-Nonce', hiplocation.api.nonce);
				},
				data: data,
				error: function (r, error_text, error) {
					console.log(r);
					console.log(error_text);
					console.log(error);
					var message = hiplocation.strings.error;
					if (r.hasOwnProperty('message')) {
						message = r.message;
					}
					$('#feedback').html('<p class="failure">' + message + '</p>');
				},
				success: function (r) {
					$('#feedback').html('<p class="success">' + hiplocation.strings.saved + '</p>');
				},

				complete: function () {
					setTimeout(function () {
						$('#feedback').fadeOut(200);
					}, 2500);
					$('.hip-settings').fadeTo(100, 1);
				}
			});
		})

		$('.upload_map_pin_icon').on('click', function (e) {
			e.preventDefault();
			var uploadMapPinIcon = $(this);
			var media_uploader;
			var imgProperty;
			media_uploader = wp.media({
				frame: "post",
				state: "insert",
				multiple: false
			});
			media_uploader.on("insert", function () {
				imgProperty = media_uploader.state().get("selection").first().toJSON();
				uploadMapPinIcon.prev('.map_pin_icon').attr('src', imgProperty.url);
				uploadMapPinIcon.next('input').val(imgProperty.url);
			});
			media_uploader.open();
		});

		$('button.remove_map_pin_icon').on('click', function (e) {
			e.preventDefault();
			$('.map_pin_icon').attr('src', '').remove();
			$('#map_pin_img').remove();
		});
		// upload map banner
		$('.upload_map_pin_banner').on('click', function (e) {
			e.preventDefault();
			var uploadMapBanner = $(this);
			var media_uploader;
			var imgProperty;
			media_uploader = wp.media({
				frame: "post",
				state: "insert",
				multiple: false
			});
			media_uploader.on("insert", function () {
				imgProperty = media_uploader.state().get("selection").first().toJSON();
				uploadMapBanner.prev('.map_pin_banner_img').attr('src', imgProperty.url);
				uploadMapBanner.next('input').val(imgProperty.url);
			});
			media_uploader.open();
		});
		$('button.remove_map_pin_banner').on('click', function (e) {
			e.preventDefault();
			$('.map_pin_banner_img').attr('src', '').remove();
			$('#map_pin_banner').remove();
		});
	}
});

function openOptions(evt, optionName) {
	// Declare all variables
	var i, tabcontent, tablinks;

	// Get all elements with class="tabcontent" and hide them
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none";
	}

	// Get all elements with class="tablinks" and remove the class "active"
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}

	// Show the current tab, and add an "active" class to the button that opened the tab
	document.getElementById(optionName).style.display = "block";
	evt.currentTarget.className += " active";
}
