<div class="hip-map-only map-wrapper">
    <div id="map_canvas"></div>
</div>

<script>
    var geocoder;
    var lat = "<?php echo $lat ?>"; // need the quotes or an empty string will throw an error
    var lng = "<?php echo $long ?>";
    var zoom = "<?php echo $zoom ?>";

    function global_initialize() {
        geocoder = new google.maps.Geocoder();
        var args = {mapTypeId: google.maps.MapTypeId.ROADMAP};
        if (lat != '' && lng != '' && zoom != '') {
            args.center = {};
            args.center.lat = parseFloat(lat);
            args.center.lng = parseFloat(lng);
            args.zoom = parseInt(zoom);
        }
        var map = new google.maps.Map(document.getElementById('map_canvas'), args);

        if (geocoder) {
            var marker = [];
            var infowindow = [];
            var fitBounds = false;
            var bounds = 'manual';
            if(lat == '' || lng == '' || zoom == '') {
               fitBounds = true;
               var bounds = new google.maps.LatLngBounds();
            }

            <?php foreach ($locations as $index => $location) : ?>
                <?php
                $address = get_post_meta($location->ID, 'location_street_address', true);
                $city = get_post_meta($location->ID, 'location_city', true);
                $state = get_post_meta($location->ID, 'location_state', true);
                $phone = get_post_meta($location->ID, 'location_phone_number', true);
                ?>
                geocoder.geocode({
                    'address': '<?php echo "$address $city, $state" ?>'
                }, function(results, status) {
                    var pin_lat = '<?php echo get_post_meta($location->ID, 'location_lat', true) ?>';
                    var pin_lng = '<?php echo get_post_meta($location->ID, 'location_lng', true) ?>';
                    var pin_width = '<?php echo rtrim($settings['map_pin_icon_size_width'], 'px')?>';
                    var pin_height = '<?php echo rtrim($settings['map_pin_icon_size_height'], 'px')?>';
                    var icon = {
                        url: '<?php echo $settings['map_pin_img']?>',
                        origin: new google.maps.Point(0,0),
                        anchor: new google.maps.Point(0,0)
                    }
                    if(pin_height != '' && pin_width != '') {
                        icon.scaledSize = new google.maps.Size(pin_width, pin_height);
                    }
                    else {
                        icon.scaledSize = new google.maps.Size(50, 50);
                    }
                    var marker_args = {
                        position: results[0].geometry.location,
                        map: map,
                        title: '<?php echo get_the_title() ?>',
                    }
                    if(icon.url != '') {
                        marker_args.icon = icon;
                    }

                    if (status == google.maps.GeocoderStatus.OK) {
                        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                            infowindow[<?php echo $index ?>] = new google.maps.InfoWindow({
                                content: '<b>' + '<?php echo get_the_title($location); ?>' + '</b><br /><?php echo $address ?><br /><?php echo "$city, $state" ?><br /><a href="tel:<?php echo $phone; ?>" class="primary-txt"><i class="fas fa-mobile-alt" style="font-size: 1em"></i> <?php echo $phone ?></a>',
                                size: new google.maps.Size(150, 50)
                            });
                            marker[<?php echo $index ?>] = new google.maps.Marker(marker_args);
                            if(bounds !== 'manual') {
                                bounds.extend(new google.maps.LatLng(pin_lat, pin_lng));
                            }
                            marker[<?php echo $index ?>].addListener('mouseover', function() {
                                infowindow.forEach(function(win, index) {
                                    win.close(map, marker[<?php echo $index ?>]);
                                });

                                infowindow[<?php echo $index ?>].open(map, marker[<?php echo $index ?>]);
                            });

                            marker[<?php echo $index ?>].addListener('click', function() {
                                map.setCenter(results[0].geometry.location);
                            });

                            if (document.querySelector('[data-index="<?php echo $index ?>"]')) {

                                document.querySelector('[data-index="<?php echo $index ?>"]').addEventListener('mouseover', function() {
                                    infowindow.forEach(function(win, index) {
                                        win.close(map, marker[<?php echo $index ?>]);
                                    });

                                    infowindow[<?php echo $index ?>].open(map, marker[<?php echo $index ?>]);
                                });
                            }
                        } else {
                            console.log("No results found");
                        }
                    } else {
                        console.log("Geocode was not successful for the following reason: " + status);
                    }
                    if(fitBounds) {
                        map.fitBounds(bounds);
                    }
                });

            <?php endforeach; ?>


        }
    }
        window.addEventListener('load', global_initialize);
</script>