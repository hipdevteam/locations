<section class="location">
    <div class="location-wrapper">
        <div class="map-locations">
            <div class="location-name-items py-4 py-md-3">
                <?php if (!empty($fields['hip_location_block_name'])) : ?>
                    <h4 class="mb-4"><?php echo $fields['hip_location_block_name']; ?></h4>
                <?php endif; ?>
                <ul>
                    <?php foreach ($locations as $index => $location) : ?>
                        <li>
                            <?php
                            $address = get_post_meta($location->ID, 'location_street_address', true);
                            $city = get_post_meta($location->ID, 'location_city', true);
                            $state = get_post_meta($location->ID, 'location_state', true);
                            $zipcode = get_post_meta($location->ID, 'location_zipcode', true);
                            $phone = get_post_meta($location->ID, 'location_phone_number', true);
                            $stateOption = get_post_meta($location->ID, 'state_option', true);
                            ?>
                            <a data-index="<?php echo $index ?>" href="<?php echo get_permalink($location) ?>">
                                <?php echo $city ?><?php if ($stateOption == 'show_state') echo ", $state"; ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
        <div class="map-wrapper">
            <div id="map_canvas"></div>
        </div>
    </div>
</section>

<script>
    var geocoder;

    function global_initialize() {
        geocoder = new google.maps.Geocoder();
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
            zoom: 8,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            center: {
                lat: 42.2755406,
                lng: -71.8779488
            }
        });

        if (geocoder) {
            var marker = [];
            var infowindow = [];

            <?php foreach ($locations as $index => $location) : ?>
                <?php
                $address = get_post_meta($location->ID, 'location_street_address', true);
                $city = get_post_meta($location->ID, 'location_city', true);
                $state = get_post_meta($location->ID, 'location_state', true);
                $phone = get_post_meta($location->ID, 'location_phone_number', true);
                ?>
                geocoder.geocode({
                    'address': '<?php echo "$address $city, $state" ?>'
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                            infowindow[<?php echo $index ?>] = new google.maps.InfoWindow({
                                content: '<b>' + '<?php echo get_the_title($location); ?>' + '</b><br /><?php echo $address ?><br /><?php echo "$city, $state" ?><br /><a href="tel:<?php echo $phone; ?>" class="primary-txt"><i class="fas fa-mobile-alt" style="font-size: 1em"></i> <?php echo $phone ?></a>',
                                size: new google.maps.Size(150, 50)
                            });

                            marker[<?php echo $index ?>] = new google.maps.Marker({
                                position: results[0].geometry.location,
                                map: map,
                                title: '<?php echo get_the_title() ?>'
                            });

                            marker[<?php echo $index ?>].addListener('mouseover', function() {
                                infowindow.forEach(function(win, index) {
                                    win.close(map, marker[<?php echo $index ?>]);
                                });

                                infowindow[<?php echo $index ?>].open(map, marker[<?php echo $index ?>]);
                            });

                            marker[<?php echo $index ?>].addListener('click', function() {
                                map.setCenter(results[0].geometry.location);
                            });

                            document.querySelector('[data-index="<?php echo $index ?>"]').addEventListener('mouseover', function() {
                                infowindow.forEach(function(win, index) {
                                    win.close(map, marker[<?php echo $index ?>]);
                                });

                                infowindow[<?php echo $index ?>].open(map, marker[<?php echo $index ?>]);
                            });
                        } else {
                            console.log("No results found");
                        }
                    } else {
                        console.log("Geocode was not successful for the following reason: " + status);
                    }
                });

            <?php endforeach; ?>


        }
    }
    window.addEventListener('load', global_initialize);
</script>