<div class="elementor-custom-embed hip-single-location-map">
<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0" height="<?php echo $height ?>"
					src="https://maps.google.com/maps?z=<?php echo $zoom?>&amp;q=<?php echo get_post_meta($location->ID, 'hip_location_address', true); ?>'&amp;t=m&amp;output=embed&amp;iwloc=near"
					title="<?php echo get_post_meta($location->ID, 'hip_location_address', true); ?>"
			></iframe>
</div>