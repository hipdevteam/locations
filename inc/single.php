<?php get_header(); ?>

<?php
	$address = get_post_meta( get_the_ID(), 'location_street_address', true );
	$city = get_post_meta( get_the_ID(), 'location_city', true );
	$state = get_post_meta( get_the_ID(), 'location_state', true );
	$new_patient_phone = get_post_meta( get_the_ID(), 'location_phone_new_patient', true );
	$address = get_post_meta( get_the_ID(), 'location_street_address', true );
	$zipcode = get_post_meta( get_the_ID(), 'location_zipcode', true );
	$city = get_post_meta( get_the_ID(), 'location_city', true );
	$state = get_post_meta( get_the_ID(), 'location_state', true );
	$google_review = get_post_meta( get_the_ID(), 'gmb_url', true);
	$facebook_review = get_post_meta( get_the_ID(), 'location_facebook_url', true);
	$phone = get_post_meta( get_the_ID(), 'location_phone_number', true );


	if (class_exists('Hip\Location\Settings')) {
		$location_settings = \Hip\Location\Settings::getSettings();
	}
?>

<div class="container single-location-container">
	<div class="column content-column">
              <h2><?php echo apply_filters('locations_address_heading', 'Address'); ?></h2>
		<p>
			<?php
				echo $address . "<br />";
				echo $city . ", ";
				echo $state;
				if ( $zipcode ) {
					echo " " . $zipcode;
				}
			?>

			<br /><br style="line-height: .5rem' />
			<i class='fas fa-mobile-alt' style='font-size: 1em'></i>
			<a href="tel:<?php echo $phone ?>"><?php echo $phone ?></a>
			<?php if ($new_patient_phone != ""): ?>
				<br />New Patients: <a href="tel:<?php echo $new_patient_phone ?>"><?php echo $new_patient_phone ?></a>;
			<?php endif; ?>
		</p>

		<?php if ( $hours = get_post_meta( get_the_ID(), 'location_hours', true ) ): ?>
			<h2><?php echo apply_filters('locations_hours_heading', 'Hours of Operation'); ?></h2>
			<p><?php echo nl2br($hours) ?></p>
		<?php endif; ?>

		<?php if ( $google_review || $facebook_review ): ?>
			<h2><?php echo apply_filters('locations_reviews_heading', 'Write a Review'); ?></h2>
			<div class="review-icon-container">
			<?php if ( $google_review ): ?>
			  <a href="<?php echo $google_review ?>"><img width="90" height="90" src="<?php echo HIP_LOCATION_PLUGIN_URL ?>/assets/google-icon.jpg"></a>
			<?php endif ?>
			<?php if ( $facebook_review ): ?>
				<a href="<?php echo $facebook_review ?>"><img width="90" height="90" src="<?php echo HIP_LOCATION_PLUGIN_URL ?>/assets/facebook-icon.jpg"></a>
			<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php if( '' !== get_post()->post_content ) {
					echo "<h2>More Info</h2>";
					echo get_post()->post_content;
				}
		?>
	</div>
	<div class="column map-column">
		<div class="button-container">
			<div class="button-column">
				<a href="<?php echo !empty($location_settings['sl_left_btn_link']) ? $location_settings['sl_left_btn_link'] : '/lp/schedule-appointment';?>" class="button-secondary location-button"><?php echo !empty($location_settings['sl_left_btn_text']) ? $location_settings['sl_left_btn_text'] : 'SCHEDULE APPOINTMENT';?></a>
			</div>

			<div class="button-column">
				<a href="<?php echo !empty($location_settings['sl_right_btn_link']) ? $location_settings['sl_right_btn_link'] : '/contact-us';?>" class="button-primary location-button"><?php echo !empty($location_settings['sl_right_btn_text']) ? $location_settings['sl_right_btn_text'] : 'HAVE A QUESTION? CONTACT US';?></a>
			</div>
		</div>
		<?php if ( $place_id = get_post_meta( get_the_ID(), 'location_place_id', true ) ): ?>
			<iframe
				style="width: 100%"
				height="450"
				frameborder="0" style="border:0"
				src="https://www.google.com/maps/embed/v1/place?key=AIzaSyC1lJQcAk0_O8gigUc43fV4HaQpXCf9lUk&q=place_id:<?php echo $place_id ?>" allowfullscreen>
			</iframe>
		<?php else: ?>
			<div class="map-container">
				<div id="single_map_canvas"></div>
			</div>
		<?php endif; ?>
	</div>
</div>
<div class="container location-reviews-container">
	<?php if ( $bid = get_post_meta( get_the_ID(), 'demandforce_id', true ) && $bslug = get_post_meta( get_the_ID(), 'demandforce_slug', true ) ): ?>
		<script>
			d3cp_bid = '<?php echo $bid ?>';
			d3cp_link_scheduler = 'lp/schedule-appointment';
		</script>
		<script src="//www.demandforced3.com/b/<?php echo $bslug ?>/reviews.widget" type="text/javascript"></script>
	<?php endif; ?>
</div>
<div class="container after-location-container">
  <?php echo get_post_meta( get_the_ID(), 'after_location_content', true ) ?>
</div>

<script>
	var geocoder;
	function initialize() {
		geocoder = new google.maps.Geocoder();
		var address = "<?php echo "$address, $city, $state" ?>";

		var map = new google.maps.Map(document.getElementById('single_map_canvas'), {
			zoom: 12,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});

		if (geocoder) {
			geocoder.geocode({
				'address': address
			}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {
					if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
					map.setCenter(results[0].geometry.location);

					var infowindow = new google.maps.InfoWindow({
						content: '<b>' + '<?php echo get_the_title(); ?>' + '</b><br /><?php echo $address ?><br /><?php echo "$city, $state" ?><br /><?php echo $phone ?>',
						size: new google.maps.Size(150, 50)
					});

					var marker = new google.maps.Marker({
						position: results[0].geometry.location,
						map: map,
						title: '<?php echo get_the_title() ?>'
					});
					infowindow.open(map, marker);

					} else {
					alert("No results found");
					}
				} else {
					alert("Geocode was not successful for the following reason: " + status);
				}
			});
		}
	}
	window.addEventListener('load', initialize);
</script>

<?php get_footer(); ?>
