<?php
	$location_settings;

	if (class_exists('Hip\Location\Settings')) {
		$location_settings = \Hip\Location\Settings::getSettings();
	}

?>


<section class="location-without-maps py-5">
	<div class="wrapper-container">
		<div class="items">
			<?php if(!empty($location_settings['location_footer_title'])): ?>
				<h2 class="<?php echo $this->checkPrimaryTxtColor(); ?>">
					<?php echo $location_settings['location_footer_title']; ?>
				</h2>
			<?php endif; ?>
			<?php
				$i = 0;
			?>
			<ul class='location-lists'>
			<?php
				foreach ( $locations as $index => $location ):

			
				// Create Variables
				$address = get_post_meta( $location->ID, 'location_street_address', true );
				$city = get_post_meta( $location->ID, 'location_city', true );
				$state = get_post_meta( $location->ID, 'location_state', true );
				$zipcode = get_post_meta( $location->ID, 'location_zipcode', true );
				$phone = get_post_meta( $location->ID, 'location_phone_number', true );
				$gmb = get_post_meta( $location->ID, 'gmb_url', true);
				$mobileGmb = get_post_meta( $location->ID, 'gmb_url_mobile', true);
				$stateOption = get_post_meta( $location->ID, 'state_option', true );
				$showInFooter = get_post_meta( $location->ID, 'location_show_in_footer', true );
				$isTextingAllowed = get_post_meta( $location->ID, 'location_texting', true );
				$rowBreak = (!empty($location_settings['location_break_row_on']) ? $location_settings['location_break_row_on'] : 3);
				
				if ( $showInFooter === "show_in_footer" ):
					$title = $this->getTitle($city, $state, $stateOption);
					if ( $i % ($rowBreak) === 0 && $i !== 0 ) echo "</ul><ul class='location-lists'>";
				?>
					<li>
						<a href="<?php the_permalink($location); ?>" class="no-underline">
							<p class="<?php $this->checkPrimaryTxtColor(); ?>"><i class="fas fa-map-marker-alt"></i></p>
							<h5 class="<?php $this->checkPrimaryTxtColor(); ?>"><?php echo $title; ?></h5>
							<p class="footer-secondary-color">
							<?php if (!empty($address)): ?>
								<?php echo $address; ?><br>
							<?php endif; ?>
							<?php if (!empty($zipcode)): ?>
								<?php echo $city . ", " . $state . " "  . $zipcode; ?>
							<?php endif; ?>
							</p>
						</a>
						<?php if (!empty($phone)): ?>
							<div class="footer_location_nonmobile phone_block">
								<p class="<?php $this->checkPrimaryTxtColor(); ?>">
									<i class="fas fa-mobile-alt" style="font-size: 1em;"></i>&nbsp;&nbsp;
									<a href="tel:<?php echo $phone; ?>"><?php echo $this->getPhoneNumber($phone); ?></a>
								</p>
							</div>
							<div class="footer_location_mobile phone_block_mobile">
								<a href="tel:<?php echo $phone; ?>" class="no-underline">
									<button class="footer-primary-call-button">
										Call <?php if ($isTextingAllowed === "yes") echo "or Text"; ?> Now
									</button>
								</a>
							</div>
							<br />
						<?php endif; ?>
						<?php if (!empty($gmb)): ?>
							<div class="footer_location_nonmobile">
								<a href="<?php echo $gmb; ?>" class="<?php $this->checkPrimaryTxtColor(); ?> gmb" target="_blank" rel="noopener">
									<i class="fab fa-google <?php $this->checkPrimaryTxtColor(); ?>"></i> 
									REVIEW US
								</a>
							</div>
							<div class="footer_location_mobile">
								<a href="<?php echo $mobileGmb ? $mobileGmb : $gmb; ?>" class="<?php $this->checkPrimaryTxtColor(); ?> gmb" target="_blank" rel="noopener">
									<i class="fab fa-google <?php $this->checkPrimaryTxtColor(); ?>"></i> 
									REVIEW US
								</a>
							</div>
						<?php endif; ?>
					</li>
			<?php
					$i++;
				endif;
			endforeach;
			?>
			</ul>
			<?php if($this->needDropdown($locations)): ?>
			<div class="locations-footer-menu">
				<ul class="view-more">
					<li>View More Locations 
						<ul>
							<?php	
								foreach ( $locations as $index => $location ):
									// Create Variables
									$city = get_post_meta( $location->ID, 'location_city', true );
									$showInFooter = get_post_meta( $location->ID, 'location_show_in_footer', true );

									if ( $showInFooter !== "show_in_footer" ): 
							?>
										<a href="<?php the_permalink($location); ?>" class="no-underline">
											<li><?php echo $city; ?></li>
										</a>
							<?php
									endif;
								endforeach;
							?>
						</ul>
					</li>
				</ul>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>
